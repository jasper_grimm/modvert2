<?php namespace Modvert\Filesystem;

/**
 * Created by PhpStorm.
 * User: vestnik
 * Date: 12/5/2015
 * Time: 2:28 AM
 */
interface IResourceReader
{

    public function read();

}